"""
Grouping
"""
import numpy as np # 科学技術計算ライブラリ
import matplotlib.pyplot as plt # 科学技術計算ライブラリ
import pandas as pd #csv読み込み
import random
# import pdb; pdb.set_trace()


"""
Global parameters
"""
groupNum = 6 # グループ数


"""
Main
"""
def main():
    # csvファイル読み込んでデータフレーム作成
    fullPathName = "/Users/3824/Desktop/prog/learningGroup/"
    pathName = fullPathName + "animalDat3.csv"
    rawDt = pd.read_csv(pathName)
    rowLength = len(rawDt.index)
    colLength = len(rawDt.columns)

    # 優先度設定
    pri1Header = 'learningStyle' # ラベルの名前で設定
    pri2Header = 'grade'
    priHeader = [pri1Header, pri2Header]
    pri1 = rawDt[pri1Header] # ラベル名の列をデータフレームないで優先度の列に設定
    pri2 = rawDt[pri2Header]
    id = rawDt['index']
    name = rawDt['name']
    Dt = pd.concat([pri1, pri2, id, name], axis=1) # 優先度順に並べ替えたデータフレーム作成
    #print("RAW DATA :\n", rawDt)
    #print("PRIORITY SET DATA :\n", Dt)


    """
    # グループの箱を用意
    classMemNum = rowLength # 行数でクラスの人数設定
    memNum = (int)(classMemNum / groupNum) # 1グループの人数設定
    if classMemNum % groupNum != 0 :# 割り切れないときは1グループの人数を1人分増やす
        memNum = memNum + 1
    GroupBox = pd.DataFrame( index=id, columns=['gourpID'] ) # 空のグループ箱を用意
    for i in range(0, groupNum):
        for j in range(0, memNum):
            #GroupBox.ix[i*memNum + j, 0] = i
            randNum = random.randint(0, RowLength - i) # (元-ランダムに取得した分)行数分だけのint乱数発生
            buf[i] = sortedDt.iloc[randNum, :] # randNum行目丸ごとbuf[i]にコピー
            GroupBox.ix[i*memNum + j, 0] =
    print("Class Member:", classMemNum, " => ", memNum, "[member] x", groupNum, " [groups]")
    print("GROUP BOX : \n", GroupBox)
    """

    classMemNum = rowLength # 行数でクラスの人数設定
    memNum = (int)(classMemNum / groupNum) # 1グループの人数設定
    if classMemNum % groupNum != 0 : # 割り切れないときは1グループの人数を1人分増やす
        memNum = memNum + 1
    GroupBox = pd.DataFrame( index=[], columns=[] ) # 空のグループ箱を用意

    #
    # グループに人数を格納
    #
    for i in range(0, groupNum):

        # 1人目はランダム
        randNum = random.randint(0, rowLength-1) # (元-ランダムに取得した分)行数分だけのint乱数発生
        GroupBox = GroupBox.append(Dt.iloc[randNum]) # ランダム選択した行をGroupBoxに追加
        Dt.drop([randNum]) # ランダム選択した行を削除
        # なぜか順番がアルファベット順?でappendされるので毎回これやんのか？
        Gpri1 = GroupBox[pri1Header] # ラベル名の列をデータフレームないで優先度の列に設定
        Gpri2 = GroupBox[pri2Header]
        Gid = GroupBox['index']
        Gname = GroupBox['name']
        GroupBox = pd.concat([Gpri1, Gpri2, Gid, Gname], axis=1) # 優先度順に並べ替えたデータフレーム作成

        # 2人目以降
        for j in range(1, memNum-1):
            priHeaderForSort = []
            for pri in range(0, colLength-2): # priHeaderの分(列数-(index+nameの2列))
                # Dt列が優先度順になってるのでGroupBox列も優先度順
                # なのでpri in 0-列数-2でpriHeaderを網羅
                lastRaw = len(GroupBox.index)
                #print("lastRaw_", lastRaw-1, ",pri_", pri, " abs:\n",
                #      abs(GroupBox.iloc[lastRaw-1,pri] - Dt.iloc[:,pri]))
                difName = 'dif_' + priHeader[pri]
                Dt[difName] = abs(GroupBox.iloc[lastRaw-1,pri] - Dt.iloc[:,pri])
                priHeaderForSort.append(difName)
                Dt = Dt.sort(priHeaderForSort, ascending=False) # 前回選んだpramとの差分で降順に並べ替え

                currentIndex = Dt['index'] # sort後のindex並び取得
                currentRowLength = len(currentIndex) # 選択削除分を覗いた現在の行数
                prob = convertProb(currentRowLength)

                # for debug
                if i==3 & j==2 :
                    print(i, j, "pri:", pri, "lastGroupBox:", GroupBox.iloc[lastRaw-1,pri],
                        "\n Dt : \n", Dt)


        print(i, ":", "GROUP BOX : \n", GroupBox)

    print("----------------------------------")
    print("Class Member:", classMemNum, " => ", memNum, "[member] x", groupNum, " [groups]")
    print("GROUP BOX : \n", GroupBox)


    #for i in range(0, groupNum)
    #    for j in range(0, memNum)
            #randomMemChoice()



    """
    # (列ごとにmean,std違うので列ごとに)標準化(正規化)
    pri1 = rawPri1.div(rawPri1.sum(1), axis=0)
    pri2 = rawPri2.div(rawPri2.sum(1), axis=0)
    nonSortedDt = pd.concat([Id, pri1, pri2], axis=1) # 優先度順に列を結合してデータフレームDt作成
    print("RAW DATA :\n", nonSortedDt)
    print("\n\n\n")
    """


    """
    # 優先度順列ごとに降順にソートしたデータフレームDt作成
    Dt = nonSortedDt.sort([Pri1Header, Pri2Header], ascending=False)
    RowLength = len(Dt.index)
    ColLength = len(Dt.columns)
    print("SORTED DATA\n", Dt, "\nRowLength: ", RowLength, "x ColLength: ", ColLength)
    print("\n\n\n")
    print(Dt.ix[0:2])
    #print(Dt.ix[:, 0:1])
    """


    # 優先度1のパラメータを水準ごと列に分けて
    # 優先度2を1行1行並べる
    #lvMargePd = devideLevel(sortedDt)
    #grouper = ['g1', 'g2', 'g3', 'g4', 'g5'] * 2
    #Dt.groupby(grouper).groups


    """
    # sortedDtのpri1列からGroupNum個ランダムに取得して一時保存
    buf = GroupNum * [0] # ランダム取得したデータ格納用(0で初期化)
    for i in range(0, GroupNum):
        randNum = random.randint(0, RowLength - i) # (元-ランダムに取得した分)行数分だけのint乱数発生
        buf[i] = sortedDt.iloc[randNum, :] # randNum行目丸ごとbuf[i]にコピー
    """



"""
End of Main
"""



"""
convertProb
1-100までで階段状に分配する
"""
def convertProb(currentRowLength):
    count = 100;
    currentIndexProb = [0]*currentRowLength # 現在残ってる候補人数分の配列
    while count > 0:
        for i in range(currentRowLength):
            currentIndexProb[i] = currentIndexProb[i] + 1
            count = count - 1
    print("prob: ", currentIndexProb)
    return currentIndexProb




"""
randomNextMemChoice
"""
#def randomNextMemChoice():






"""
尺度ごとの水準わけ
"""
def devideLevel(sortedDt):
    r = 0 # 水準ごとに分けた列それぞれの行数
    c = 0 # 水準ごとに分けた列数(=水準数)
    lvMargePd = pd.DataFrame(index=[], columns=[]) # 列を結合していくための空データフレーム
    #lvInput = ['NaN']*len(sortedDt) # sortedの水準ごとの列(後々のためNanで初期化, 十分な列数確保)
    #lvInputId = pd.DataFrame(index=[], columns=[])
    #lvInput = pd.DataFrame(index=[], columns=[])
    lvInputId = ['NaN']*len(sortedDt) # sortedの水準ごとの列(後々のためNanで初期化, 十分な列数確保)
    lvInput = ['NaN']*len(sortedDt) # sortedの水準ごとの列(後々のためNanで初期化, 十分な列数確保)


    # sortedDtの優先度1列でループ
    for i in range(len(sortedDt)-1) :
        lvInputId[r] = sortedDt.iloc[i, 0]
        lvInput[r] = sortedDt.iloc[i, 2] # i行目1列分書き込み
        r = r+1 # 水準ごと列の行をインクリメント
        # 水準が下がってたら
        if sortedDt.iloc[i,1] > sortedDt.iloc[i+1,1] :
            #print("lv down:", i,"と",i+1 )
            r = 0 # 行をリセット(次の水準はまた0行目から)
            colNameId = "[" + str(c) + "]" # 水準の列名は優先度1列の水準数
            colName = str(sortedDt.iloc[i,1])
            lvMargePd[colNameId] = pd.Series(lvInputId) # さっき名付けた水準に, 保存してたデータlvInput書き込み
            lvMargePd[colName] = pd.Series(lvInput)

            c = c+1 # 列数をインクリメント

        # 配列カウントの最後まで来たら
        if i+1 == len(sortedDt)-1 :
            lvInputId[r] = sortedDt.iloc[i+1, 0]
            lvInput[r] = sortedDt.iloc[i+1, 2] # i行目1列分書き込み
            colNameId = "[" + str(c) + "]" # 水準の列名は優先度1列の水準数
            colName = str(sortedDt.iloc[i,1])
            lvMargePd[colNameId] = pd.Series(lvInputId) # さっき名付けた水準に, 保存してたデータlvInput書き込み
            lvMargePd[colName] = pd.Series(lvInput) # 保存してた残りのlvInput書き込み

    print("DEVIDED DATA BY LEVELS : \n", lvMargePd) # Lv別の統合結果表示
    return lvMargePd




"""
mainを呼び出す！
"""
main()
